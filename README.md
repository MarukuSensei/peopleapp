# PEOPLE desktop player
Because there is none currently, and I just hate to have some music playing in my actual browser, I decided to create this very basic electron app in order to have a proper desktop app.

## What platforms are currently supported ?

Only Windows, because I don't have the time to adapt it for MacOS. 

**It will be available on Linux and MacOS very soon**

## How to install.

You can either build from source or download the latest wisard [here](https://gitlab.com/MarukuSensei/peopleapp/tags).

## I'm not affiliated with PEOPLE in any way, I just like what they are doing and their philosophy of music.